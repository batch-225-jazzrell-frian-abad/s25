// [SECTION] JSON
/*
	- JSON stands for Javascript Object Notation
	- JSON is also used in other programming languages hence the name JavaScript Object Notation

	Syntax
	{
		"propertyA": "valueA",
		"propertyB": "valueB",


	}

*/

//JSON Objects

// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// [SECTION] JSON Arrays
/*
"cities": [
    { "city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
    { "city": "Manila City", "province": "Metro Manila", "country": "Philippines" },
    { "city": "Makati City", "province": "Metro Manila", "country": "Philippines" }
]
*/

// [SECTION] JSON METHODS
// - the JSON object contains method for parsing and converting data into stringified JSON.

// [SECTION] Converting Data into Stringified JSON

/*
	- Stringified JSON is Javascript object converted into string to be used in other functions of a javascript application.
	- They are commonly used in HTTP request where information is required to be sent and received in a stringified JSON format
	- Request are an important part of programming where application communictes with a backend application to perform different tasks such as getting/creating data in a database.
*/

let batchesArr = [{batchName: 'Batch X'}, {batchName: 'Batch Y'}];

// The "Stringify" method is used to convert JavaScript Objects into a string.
console.log('Result from stringify method:');
console.log(JSON.stringify(batchesArr));

// Through Objects stringify

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
});

console.log(data);

//[SECTION] Using Stringify Method with Variables
/*
	Syntax

	JSON.stringify({
		propertyA: variableA,
		propertyB: variableB
	})
*/

// user details

// let firstName = prompt('What is your first name?');
// let lastName = prompt('What is your last name?');
// let age = prompt('What is your age?');

// let address = {
// 	city: prompt('Which city do you live in?'),
// 	country: prompt('Which country does your city address belong to?')
// };

// let otherData = JSON.stringify({
// 	firstname: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// })

// console.log(otherData);

// [Section] Converting stringified JSON into JavaScript objects
/*
	- Objects are common data types used in applications because of the complex data structures that can be created out of them
	- Information is commonly sent to applications in stringified JSON and then converted back into objects
	- This happens both for sending information to a backend application and sending information back to a frontend application
	-Parsing means analyzing and converting a program into an internal format that a runtime environment can actually run
*/

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log('Result from parse method:');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{ "name": "John", "age": "31", "address": { "city": "Manila", "country": "Philippines" } }`

console.log(JSON.parse(stringifiedObject));

